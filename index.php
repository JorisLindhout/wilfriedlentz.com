<?php get_header(); ?>
<!-- featured posts -->
<section id="featured">
    <?php
    $query = new WP_Query(array(
		'post_type' => array('wil_exhibition', 'wil_artist', 'post'),
		'post_status' => 'publish',
		'orderby' => 'featured_index',
		'order' => 'ASC',
		'meta_query' => array(
			'is_featured' => array(
				'key' => 'wil_featured_post',
				'value' => 'on',
				'compare' => '='
			),
			'featured_index' => array(
				'key' => 'wil_featured_index',
				'type' => 'NUMERIC'
			)
		),
		'suppress_filters' => true,
    ));
    if ($query->have_posts()) {
		$index = 1;
		while ( $query->have_posts() ) {
			$query->the_post();
			$size = '';
			$image_url = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), '');
			$image_w = $image_url[1];
			$image_h = $image_url[2];

			if ($image_w > $image_h) {
				$size = 'landscape';
			} else if ($image_w == $image_h) {
				$size = 'square';
			} else {
				$size = 'portrait';
			}

			$title = null;
			$ordering_index = get_post_meta(get_the_id(), 'wil_featured_index', true);
			if (get_post_type() != 'wil_exhibition') {
				$title = '<h2 class="title uppercase">'.get_the_title().'</h2>';
			}
			$url = get_the_permalink();
			$new_url = apply_filters('wil_featured_permalink', $url, get_the_ID());
			$link_changed = $url !== $new_url;
			$url = $new_url;
			$link_target = $link_changed ? ' target="_blank"' : "";
			$extra_classes = $link_changed ? ' external-link' : "";
			$link_anchor_name = $link_changed ? "Link" : "Info";
	?>
	<article id="slide-<?= $index ?>" class="slide <?=$size?>">
		<figure class="responsive-figure">
			<a href="<?= $url ?>"<?= $link_target ?>>
				<?=get_the_post_thumbnail(get_the_ID(),'wil-large')?>
			</a>
		</figure>
		<header>
			<a class="slide-link<?= $extra_classes ?>" href="<?= $url ?>"<?= $link_target ?>>
				<?php $title ? print($title) : include(locate_template('exhibition-title.php', false, false)) ?>
				<p class="exhibition-info"><span><?= $link_anchor_name ?></span></p>
			</a>
		</header>
	</article>
	<?php
	$index++;
	}
	} else {
		echo 'No results';
	}
	?>
</section>
<!-- /featured posts -->

<?php get_footer(); ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		
	<!-- Meta -->
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="<?php if ( is_single() ) {
			global $post;
	        single_post_title('', true); 
			echo ": ". strip_tags($post->post_content);
	    } else {
	        bloginfo('name'); 
			echo ": "; 
			bloginfo('description');
	    }
	?>">
	<meta name="keywords" content="<?php 
		$tags = get_tags();
		$keywords = '';
		foreach ( $tags as $tag ) {
			$keywords .= $tag->name.', ';
		}
		echo $keywords;
	?>">
	<meta name="author" content="<?
		global $post;
		$author = get_the_author_meta( 'display_name', $post->ID );
		if(empty($author)){
			echo ' ';
		}else{
			echo $author;
		}
	?>">
	
	<link rel="canonical" href="<?php
		global $post;
		echo get_permalink($post->ID);
	?>">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" >
	
	<!--FACEBOOK-->
	<meta property="og:title" content="<?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?>">
	<meta property="og:site_name" content=" ">
	<meta property="og:url" content="<?php
		global $post;
		echo get_permalink($post->ID);
	?>">
	<meta property="og:description" content="<?php if ( is_single() ) {
			global $post;
	        single_post_title('', true); 
			echo ": ". strip_tags($post->post_content);
	    } else {
	        bloginfo('name'); 
			echo ": "; 
			bloginfo('description');
	    }
	?>">
	<meta property="og:image" content="<?php if ( is_single() ) {
			echo get_the_post_thumbnail_url( get_queried_object_id() );
		}else{
			echo get_template_directory_uri().'/images/logo.png';
		}
	?>">
	<meta property="fb:admins" content=" ">
	<meta property="og:type" content="website">
	<meta property="og:locale" content="nl_NL">
		
	<?php wp_head(); ?>

	<noscript>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/noscript.css" media="screen">
	</noscript>
	<!--
	Development by Joris Lindhout: http://www.grok-projects.com
	-->
</head>
<body <?php body_class(); ?>>
	
	<!-- Header -->
	<header id="header">
		<!-- Logo -->
		<a href="/" title="home" id="logo-header" class="logo blue uppercase">
			<h1><?php echo get_bloginfo( 'name' ); ?></h1>
		</a>
		<!-- /Logo -->
		<a href="#header" title="Show navigation" class="menu-responsive">
			<img src="<?=get_template_directory_uri()?>/images/hamburger.svg">
		</a>
		<a href="#!" title="Hide navigation" class="menu-responsive hide-nav">
			<img src="<?=get_template_directory_uri()?>/images/hamburger-close.svg" class="active">
		</a>
		<!-- Main menu -->
		<?php wp_nav_menu( array('menu' => 'Main Menu', 'theme_location' => 'main-menu', 'container_id' => 'header-menu', 'container_class' => 'grey', 'items_wrap' => '<ul id="menu-main" class="%2$s">%3$s</ul>' )); ?>
		<br class="clearfix">
		<!-- /Main menu -->
	</header>
	<!-- /Header -->

	<main>

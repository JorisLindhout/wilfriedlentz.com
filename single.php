<?php get_header(); ?>

	<?php if ( have_posts() ) : the_post(); ?>
		<?php
			$cat_ids = get_the_category();
			$artist = null;
			foreach($cat_ids as $id){
				$artist = get_page_by_title(get_cat_name($id->term_id), OBJECT, 'wil_artist');
				if ($artist) {
					$artist_id = $artist->ID;
					$category_title = get_the_title($artist_id);
				} else {
					$category_title = get_cat_name($id->term_id);
				}
			}
		?>
		<!-- single news page -->
		<aside class="artist-menu">
			<h2 class="artist-name"><?= $category_title ?></h2>
			<?php
				if ($artist) {
					include(locate_template('single-artist-menu.php', false, false));
				}
			?>
		</aside>
		<article id="news-page" class="artist-page-content">
			<header><h2><?= get_the_title() ?></h2></header>
			<div class="article-content">
				<?php the_content() ?>
			</div>
		</article>
		
		<br class="clearfix">
		<!-- /single news page  -->
	
	<?php endif; ?>

<?php get_footer(); ?>

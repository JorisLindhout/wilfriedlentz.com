jQuery(document).ready(function($){	
	
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;

	$(window).scroll(function(event){
	    didScroll = true;
	});

	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 200);

	function hasScrolled() {
	    var st = $(this).scrollTop();

	    if(Math.abs(lastScrollTop - st) <= delta)
	        return;
	    if(st < 0)
	        return;

	    if (st > lastScrollTop){
	        $('body > header').removeClass('nav-down').addClass('header-up');
	    } else {
	        if(st + $(window).height() < $(document).height()) {
	            $('body > header').removeClass('header-up').addClass('nav-down');
	        }
	    }

	    lastScrollTop = st;
	}
	
	$('.animated-scroll').on('click',function(e){
		e.preventDefault();
		id = $(this).attr('href').slice(1);
		$('html,body').animate({scrollTop:$('body').find('#' + id + '').position().top-60}, 1000,'swing');
	})
	
	function fadeOut() {
		$('#cover img').fadeOut();
		$('body').removeClass('single-wil_artist');
	}

	function showBio() {
		fadeOut();
		$('.tab-content, .tab').removeClass('active');
		$(this).addClass('active');
		$('#bio').addClass('active');
	}

	$('.tab').on('click', function(e){
		e.preventDefault();
		//fadeOut();
		$('.single-wil_work #work, .single-wil_text #text, .single-post #news').hide();
		$('.single-wil_work .tab.work, .single-post .tab.news,.single-wil_text .tab.text').css({'color':'#000','border':'none'});
		var isActive = $(this).hasClass('active');
		$('.artist-menu, .tab-content, .tab, .show-bio').removeClass('active');
		if (!isActive) {
			$(this).removeAttr('style').addClass('active');
			$($(this).attr('href')).removeAttr('style').addClass('active');
			$('.artist-menu').addClass('active');
		}
	});
	
	$('.show-bio')
		.on('click', function (e) {
			showBio.call(this);
		})
		.each(function () {
			if(window.location.hash=='#bio'){
				showBio.call(this);
			}
		});
});


var ajaxurl = "//wilfriedlentz.com/wp-admin/admin-ajax.php";

jQuery(document).ready(function($){	
	var mediaUploader;
	$('.add-media').on('click',function(e){
		e.preventDefault();

		// If the uploader object has already been created, reopen the dialog
		if (mediaUploader) {
			mediaUploader.open();
			return;
		}
		// Extend the wp.media object
		mediaUploader = wp.media.frames.file_frame = wp.media({title: 'Choose Image', multiple: false });

		// When a file is selected, grab the URL and set it as the text field's value
		mediaUploader.on('select', function() {
			attachment = mediaUploader.state().get('selection').toJSON();
			$.each(attachment, function( index, value ){
				$(".artist_images").append('<div data-id="'+value['id']+'"><input type="hidden" name="wil_artist_images[]" value="'+value['id']+'"><img src="'+value['url']+'" width="215" height="auto"><br><a class="remove-media"><span class="dashicons dashicons-minus"></span></a></div>');
			});
			mediaUploader.escape();
		});
		// Open the uploader dialog
		mediaUploader.open();
	})

	$(document).on('click','.remove-media',function(){
		var parentContainer = $(this);
		parentContainer.parent().hide().remove(); 
		/*jQuery.ajax({
			type: 'POST',
			dataType: 'html',
			url: ajaxurl,
			data: {action:'remove_media',id:parentContainer.parent().attr('data-id')},
	        success: function(server_response){
				parentContainer.parent().hide().remove(); 
				console.log(server_response);
			},
			error: function(jqXHR, textStatus, errorThrown) {
	            console.log(errorThrown);  
	        }
		});*/
	})
	
	$(document).on('click','.more',function(){
		$(this).parent().find('.item:first-child').clone().prependTo($(this).parent().find('.container')).find("input[type='text'], input[type='date'], select, textarea").val("");
	})
	$(document).on('click','.less',function(){
		$(this).parent().fadeOut().remove();
	})
	
	$('.sortable').sortable();
	
});

<?php get_header(); ?>

	<?php if ( have_posts() ) : the_post(); ?>

		<!-- single work page -->
		<aside class="artist-menu">
			<?php 
				$artist_id = get_post_meta(get_the_ID(), 'wil_artist_id', true);
				echo '<h2 class="artist-name">'.get_the_title($artist_id).'</h2>';
				include(locate_template('single-artist-menu.php', false, false)); 
			?>
		</aside>
		<article id="work-page" class="artist-page-content">
			<?php if (has_post_thumbnail()): ?>
			<figure class="responsive-figure">
				<?php
					the_post_thumbnail('wil-large');
				?>
				<figcaption class="work-featured-thumb">
					<?php
					the_post_thumbnail_caption();
					?>
				</figcaption>
			</figure>
			<?php endif; ?>
			<header>
				<h2><?php
					$content = get_the_content();
					if (!empty(trim(strip_tags($content, '<img>')))) {
						the_title();
					}
					?></h2>
			</header>
			<div class="article-content">
			<?php the_content() ?>
			<?php
			$images = get_post_meta(get_the_ID(), 'wil_artist_images', true);
			foreach($images as $image){
				echo '<figure class="clearfix">';
				echo wp_get_attachment_image( $image, 'wil-medium', false, array( "data-caption" => "caption-".$image ) );
				echo '<figcaption>'.get_post_field('post_excerpt', $image).'</figcaption>';
				echo '</figure>';
			}
			?>
			</div>
		</article>
		
		<br class="clearfix">
		<!-- /single work page  -->
	
	<?php endif; ?>

<?php get_footer(); ?>

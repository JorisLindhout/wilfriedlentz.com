<?php if ( have_posts() ) : the_post(); ?>
	
	<?php
		$wil_link = get_post_meta(get_the_ID(), 'wil_link', true);
		$wil_link_text = get_post_meta(get_the_ID(), 'wil_link_text', true);
		/*$upcoming_start_dates = get_post_meta(get_the_ID(), 'upcoming_start_dates', true);
		$upcoming_end_dates = get_post_meta(get_the_ID(), 'upcoming_end_dates', true);
		$upcoming_descriptions = get_post_meta(get_the_ID(), 'upcoming_descriptions', true);*/
		$upcoming_items = get_post_meta(get_the_ID(), 'upcoming_items', true);
		$upcoming_dateless = get_post_meta(get_the_ID(), 'upcoming_dateless', true);
		/*$other_events_start_dates = get_post_meta(get_the_ID(), 'other_events_start_dates', true);
		$other_events_end_dates = get_post_meta(get_the_ID(), 'other_events_end_dates', true);*/
		$other_events_artists = get_post_meta(get_the_ID(), 'other_events_artists', true);
		/*$other_events_descriptions = get_post_meta(get_the_ID(), 'other_events_descriptions', true);*/
		$other_events_items = get_post_meta(get_the_ID(), 'other_events_items', true);
		$other_events_no_artist = get_post_meta(get_the_ID(), 'other_events_no_artist', true);
	?>
	
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	    <head>
	        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	        <title><?php the_title(); ?></title>
		
		
		<style type="text/css">
			@font-face {
			    font-family: 'sofiapro-regular';
			    src: url('css/fonts/sofiapro-regular-webfont.woff2') format('woff2'),
			         url('css/fonts/sofiapro-regular-webfont.woff') format('woff');
			    font-weight: normal;
			    font-style: normal;
			}
			@font-face {
			    font-family: 'sofiapro-bold';
			    src: url('css/fonts/sofiapro-bold-webfont.woff2') format('woff2'),
			         url('css/fonts/sofiapro-bold-webfont.woff') format('woff');
			    font-weight: normal;
			    font-style: normal;
			}
			@font-face {
			    font-family: 'sofiapro-boldit';
			    src: url('css/fonts/sofiapro-boldit-webfont.woff2') format('woff2'),
			         url('css/fonts/sofiapro-boldit-webfont.woff') format('woff');
			    font-weight: normal;
			    font-style: normal;
			}
			@font-face {
			    font-family: 'sofiapro-regularit';
			    src: url('css/fonts/sofiapro-regularit-webfont.woff2') format('woff2'),
			         url('css/fonts/sofiapro-regularit-webfont.woff') format('woff');
			    font-weight: normal;
			    font-style: normal;
			}

			html, body {
				font-family: 'sofiapro-regular', sans-serif;
				font-size: 14px;
				line-height: 1.5em;
			}

			strong, i {font-family:'sofiapro-bold', sans-serif;}
			em, b {font-family:'sofiapro-regularit', sans-serif;}
			strong em, i em, b em, b em {font-family:'sofiapro-boldit', sans-serif;}
		
	
			#outlook a{
				padding:0;
			}
			a {text-decoration:none;color:#000;}
			.main_content a {color:#0049ff;text-decoration:none !important;border-bottom:1px solid #0049ff;}
			a[x-apple-data-detectors=true] {
			 color: inherit !important;
			 text-decoration: inherit !important;
			}
			body{
				width:100% !important;
				-webkit-text-size-adjust:none;
				margin:0;
				padding:0;
			}
			img{
				border:none;
				height:auto;
				line-height:100%;
				outline:none;
				text-decoration:none;
			}
			#backgroundTable{
				height:100% !important;
				margin:0;
				padding:0;
				width:100% !important;
			}
			body{
				background-color:#fff;
			}
			#backgroundTable{
				background-color:#fff;
			}
			h1,h2,h3,h4,h5,h6 {
				color:#000;
				display:block;
				font-weight:normal;
				line-height:100%;
				margin-bottom:10px;
				text-align:left;
			}
		
			.preheaderContent div{
				color:#000;
				line-height:100%;
				text-align:left;
			}
			.preheaderContent div a:link,.preheaderContent div a:visited{
				color:#000;
				font-weight:normal;
				text-decoration:underline;
			}
			#templateHeader{
				background-color:#fff;
				border-bottom:0;
			}
			.headerContent{
				color:#000;
				padding:40px 0 0 0;
				text-align:left;
				vertical-align:middle;
			}
			.headerContent a:link,.headerContent a:visited{
				color:#000;
				font-weight:normal;
				text-decoration:underline;
			}
			#templateContainer,.bodyContent{
				background-color:#fff;
			}
			.bodyContent{}
			.bodyContent div{
				color:#000;
				line-height:150%;
				text-align:left;
			}
			.bodyContent div a:link,.bodyContent div a:visited{
				color:#000;
				font-weight:normal;
				text-decoration:underline;
			}
			.bodyContent img{
				display:inline;
				margin-bottom:10px;
			}
			#templateFooter{
				background-color:#fff;
				border-top:0;
			}
			.footerContent div{
				color:#000;
				line-height:125%;
				text-align:left;
			}
			.footerContent div a:link,.footerContent div a:visited{
				color:#000;
				font-weight:normal;
				text-decoration:underline;
			}
			#utility{
				background-color:#fff;
			}
			#utility div{
				text-align:center;
			}
			blockquote {font-size:18px;margin-left: 0;}
		@media only screen and (max-width: 500px){
			.content-column{
				width:100% !important;
				float:none !important;
				margin:0 !important;
			}
			.footer .content-column{
				text-align:center !important;
				margin:0 0 25px 0 !important;
			}

	}</style></head>
	    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
	    	<center>
	        	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="backgroundTable">
	            	<tr>
	                	<td align="center" valign="top" width="100%">
	                    	<table border="0" cellpadding="0" cellspacing="0" style="width:calc(100% - 30px);padding:0 15px;max-width:1000px;" id="templateContainer">
	                        	<tr>
	                            	<td align="center" valign="top">
	                                    <!-- // Begin Template Header \\ -->
	                                	<table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding: 0px 5% 0 5%;" id="templateHeader">
	                                        <tr>
	                                            <td class="headerContent">
													<img src="<?php echo get_template_directory_uri(); ?>/images/mailing/header.png" style="width:100% !important;max-width:413px !important;height:auto;" width="413">
												</td>
	                                        </tr>
	                                    </table>
	                                    <!-- // End Template Header \\ -->
	                                </td>
	                            </tr>
	                        	<tr>
	                            	<td align="center" valign="top">
	                                    <!-- // Begin Template Body \\ -->
	                                	<table border="0" cellpadding="10" cellspacing="0" style="width:100%;" id="templateBody">
											<tr>
	                                            <td valign="top" class="bodyContent">
                                
	                                                <!-- MAIN CONTENT -->
	                                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;margin-bottom:50px;padding-bottom: 50px;border-bottom:2px dashed #0049ff;padding: 0px 5% 50px 5%;">
														<tr>
															<td>
																<img style="margin-left: -5%;width: 110%;height:auto;" src="<?php echo get_the_post_thumbnail_url(); ?>">
															</td>
														</tr>
	                                                    <tr>
	                                                        <td valign="top" style="padding-top:40px">
	                                                            <h2 style="color:#0049ff;font-weight:normal;margin:0;padding:0;font-size:28px;"><?php the_title(); ?></h2>   	
																<div class="main_content" style="font-size: 14px;"><?php the_content(); ?></div>
																<a href="<?php echo $wil_link ?>" style="color:#0049ff;text-decoration:none;border-bottom:1px solid #0049ff;font-size: 14px;">&gt; <?php echo $wil_link_text ?></a>
															</td>
	                                                    </tr>
													</table>
													<!-- /MAIN CONTENT -->
												
													<?php
													$total_upcoming = 0;
														if(is_array($upcoming_descriptions)&&count($upcoming_descriptions)>0&&!empty($upcoming_descriptions[0])){
														$total_upcoming = count($upcoming_descriptions);
													}
													if(trim($upcoming_dateless)!=''){
														$total_upcoming++;
													}
													if($total_upcoming>0){
													?>
													<!-- UPCOMING -->
	                                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;margin-bottom:50px;padding-bottom: 50px;border-bottom:2px dashed #0049ff;padding: 0px 5% 50px 5%;">
	                                                    <tr>
	                                                        <td valign="top">
																<h3 style="color:#0049ff;font-weight:normal;margin:0;padding:0;font-size: 28px;">Upcoming</h3>
																<?php
																	/*for($x=0;$x<count($upcoming_descriptions);$x++){
																		$start_date = $upcoming_start_dates[$x];
																		$end_date = $upcoming_end_dates[$x];
																		if (!$start_date) $start_date = time();
																	    if (!$end_date)   $end_date = $start_date + (24 * 60 * 60);

																	    $start_date = date('d-m-Y', $start_date);
																	    $end_date = date('d-m-Y', $end_date);
																		echo '<br>';
																    	echo '<div style="width:200px;margin-right:10px;float:left;font-size: 14px;" class="content-column">'.$start_date .' &mdash; '.$end_date.'</div>';
																    	echo '<div style="width: calc(100% - 210px);min-width:200px;float:left;font-size: 14px;" class="content-column">'. $upcoming_descriptions[$x] .'</div>';
																		echo '<br style="display:block;clear:both;">';
																	}*/
																	echo '<div style="width: 100%;min-width:200px;font-size: 14px;" class="content-column">'. nl2br($upcoming_items) .'</div>';
																?>
																<div style="padding-top: 14px;font-size: 14px;">
	                                                                <?php echo nl2br($upcoming_dateless); ?>
	                                                            </div>
															</td>
	                                                    </tr>
													</table>
													<!-- /UPCOMING -->
													<?php } ?>
															
													<?php
													$total_other_events = 0;
															if(is_array($other_events_descriptions)&&count($other_events_descriptions)>0&&!empty($other_events_descriptions[0])){
														$total_other_events = count($other_events_descriptions);
													}
													if(trim($other_events_no_artist)!=''){
														$total_other_events++;
													}
													if($total_other_events>0){
													?>
													<!-- OTHER EVENTS -->
	                                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;margin-bottom:50px;padding-bottom: 50px;border-bottom:2px dashed #0049ff;padding: 0px 5% 50px 5%;">
	                                                    <tr>
	                                                        <td valign="top">
																<h3 style="color:#0049ff;font-weight:normal;margin:0;padding:0;font-size:28px;">Other events</h3>  
																<?php
																	$artists = array();
																	for($x=0;$x<count($other_events_items);$x++){
																		
																		/*$start_date = $other_events_start_dates[$x];
																		$end_date = $other_events_end_dates[$x];
																		if (!$start_date) $start_date = time();
																	    if (!$end_date)   $end_date = $start_date + (24 * 60 * 60);

																	    $start_date = date('d-m-Y', $start_date);
																	    $end_date = date('d-m-Y', $end_date);
																		
																		$content = '<div style="width:200px;margin-right:10px;float:left;font-size: 14px;" class="content-column">'.$start_date.' &mdash; '.$end_date.'</div>';
																		$content .= '<div style="width:calc(100% - 210px);min-width:200px;float:left;font-size: 14px;" class="content-column">'.$other_events_descriptions[$x].'</div>';
																		$content .= '<br style="display:block;clear:both;">';*/
																		
																		$content = '<div style="width:100%;font-size: 14px;" class="content-column">'.nl2br($other_events_items[$x]).'</div>';
																		
																		if(isset($artists[$other_events_artists[$x]])){
																			$artists[$other_events_artists[$x]] = $artists[$other_events_artists[$x]].$content;
																		}else{
																			$artists[$other_events_artists[$x]] = $content;
																		}
																	}
																																		
																	foreach($artists as $key => $artist){
																		$artist_name = get_the_title($key);
																		?>
																			<br style="display:block;clear:both;">
																			<h4 style="font-weight:bold;font-size: 16px;margin:10px 0 0 0;width: 100%;clear: both;"><?php echo $artist_name; ?></h4>
																		<?
																		echo $artist;
																	}
																?>
																<div style="padding-top: 14px;font-size: 14px;">
	                                                                <?php echo nl2br($other_events_no_artist); ?>
	                                                            </div>
															</td>
	                                                    </tr>
													</table>
													<!-- /OTHER EVENTS -->
													<?php } ?>
									
													<table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding: 0px 5% 0 5%;font-size: 14px;">
														<tr>													
															<td valign="top">
																<table style="width:100%;">
																	<tbody>
																		<tr>
																			<td valign="top" class="footer">
																				<div style="width:33%;float:left;" class="content-column">
																					<a href="http://wilfriedlentz.com" style="color:#0049ff;text-decoration:none;border-bottom:1px solid #0049ff;">wilfriedlentz.com</a><br />
																					<a href="mailto:office@wilfriedlentz.com" style="color:#0049ff;text-decoration:none;border-bottom:1px solid #0049ff;">office@wilfriedlentz.com</a><br />
																					<a href="https://www.instagram.com/wilfriedlentz/" style="color:#0049ff;text-decoration:none;border-bottom:1px solid #0049ff;">Follow on Instagram</a>
																				</div>
																				<div style="width:29%;padding:0 2% 0 2%;float:left;" class="content-column">
																					Justus van Effenstraat 130<br />
																					3027 TM Rotterdam<br />
																					The Netherlands
																				</div>
																				<div style="width:33%;float:left;" class="content-column">
																					&copy; the Artists and Wilfried Lentz
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td valign="top" colspan="3" style="padding-top: 50px;">
																				<img src="<?php echo get_template_directory_uri(); ?>/images/mailing/footer.png" style="width:100% !important;max-width:413px !important;height:auto;" width="413">
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</table>
												
	      										</td>
											</tr>
										</table>
	                                </td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
	            </table>
	        </center>
	    </body>
	</html>

<?php endif; ?>

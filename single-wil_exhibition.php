<?php get_header(); ?>

	<?php if ( have_posts() ) : the_post(); ?>
		<!-- single exhibitions page -->
		<article>
			<figure class="responsive-figure">
				<?php the_post_thumbnail('wil-medium') ?>
			</figure>
			<header>
				<?php include(locate_template('exhibition-title.php', false, false)) ?>
			</header>
			<div class="article-content">
				<?php the_content(); ?>
				<?php
				$images = get_post_meta(get_the_ID(), 'wil_artist_images', true);
				foreach($images as $image){
					echo '<figure>';
					echo wp_get_attachment_image( $image, 'wil-medium', false, array( "data-caption" => "caption-".$image ) );
					echo '<figcaption>'.get_post_field('post_excerpt', $image).'</figcaption>';
					echo '</figure>';
				}
				?>
			</div>
		</article>
		<!-- /single exhibitions page  -->
	<?php endif; ?>

<?php get_footer(); ?>

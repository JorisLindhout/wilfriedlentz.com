<?php get_header(); ?>

	<?php if ( have_posts() ) : the_post(); ?>

		<!-- single text page -->
		<aside class="artist-menu">
			<?php 
				$artist_id = get_post_meta(get_the_ID(), 'wil_artist_id', true);
			echo '<h2 class="artist-name">'.get_the_title($artist_id).'</h2>';
			include(locate_template('single-artist-menu.php', false, false)); 
			?>
		</aside>
		<article id="text-page" class="artist-page-content">
			<header><h2><?= get_the_title() ?></h2></header>
			<div class="article-content">
				<?php the_content(); ?>
			</div>
		</article>
		
		<br class="clearfix">
		<!-- /single text page  -->
	
	<?php endif; ?>

<?php get_footer(); ?>

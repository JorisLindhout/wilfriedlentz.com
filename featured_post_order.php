<?php
require_once(ABSPATH . 'wp-admin/admin.php');
?>
<div class="wrap">
	<h1><?= get_admin_page_title() ?></h1>
	<form method="post" action="<?= esc_html(admin_url('admin-post.php'))?>">
		<table class="widefat striped">
			<thead>
				<tr>
					<th class="manage-column">Featured?</th>
					<th class="manage-column">Post index</th>
					<th class="manage-column">Post title</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$query = new WP_Query(array(
					'post_type' => array('wil_exhibition', 'wil_artist', 'post'),
					'post_status' => 'publish',
					'orderby' => 'featured_index',
					'order' => 'ASC',
					'meta_query' => array(
						'is_featured' => array(
							'key' => 'wil_featured_post',
							'value' => 'on',
							'compare' => '='
						),
						'featured_index' => array(
							'key' => 'wil_featured_index',
							'type' => 'NUMERIC'
						)
					),
					'suppress_filters' => true,
				));
				foreach ($query->get_posts() as $post) {
					$id = $post->ID;
					$link = get_edit_post_link($post);
					$title = get_the_title($post);
					$index = get_post_meta($id, 'wil_featured_index', true);
					echo <<<EOHTML
<tr>
	<td><input type=checkbox name="wil_featured_post[$id][featured]" checked /></td>
	<td><input type=number name="wil_featured_post[$id][index]" value="$index" /></td>
	<td><a href="$link">$title</a></td>
</tr>
EOHTML;
				}
				?>
			</tbody>
		</table>
		<input type="hidden" name="action" value="wil_featured_post_order_save">
		<?php
		wp_nonce_field('wil_featured_post_order_nonce', 'wil_featured_post_order_nonce');
		submit_button();
		?>
	</form>
</div>

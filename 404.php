<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="404: Page not found.">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" media="screen">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" media="screen">
	<!--
	Development by Joris Lindhout: http://www.grok-projects.com
	-->
</head>
<body>

	<div id="content-404" role="main">

		<header><h1>404<br />PAGE NOT FOUND</h1></header>
		
		<div>The page you are trying to access doesn't exist.</div>
		<hr />
		<a class="home-link" href="/">wilfriedlentz.com</a>

	</div>

	<!-- Optimised Asynchronous Google Analytics -->
	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-61248973-1', 'auto');
		ga('send', 'pageview');
	</script>

</body>
</html>
<?php
$artist_id = get_post_meta(get_the_ID(), 'wil_artist_id', true);
if (empty($artist_id)) {
	// Fallback to title.
	$title = get_the_title();
} else {
	$title = get_the_title($artist_id);
}
$start = get_post_meta(get_the_ID(), 'wil_exhibition_start', true);
$end = get_post_meta(get_the_ID(), 'wil_exhibition_end', true);
if (is_single() || is_home()) {
	$heading_level = 2;
} else {
	$heading_level = 3;
}
?>
<h<?= $heading_level ?> class="title uppercase"><?= $title ?> <time class="exhibition-start exhibition-date" datetime="<?=date('Y-m-d', $start)?>"><?=date('d.m.y', $start)?></time> <time class="exhibition-end exhibition-date" datetime="<?=date('Y-m-d', $end)?>"><?=date('d.m.y', $end)?></time></h<?= $heading_level ?>>

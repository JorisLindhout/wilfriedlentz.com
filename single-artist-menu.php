	<?php
	$args = array(
		'posts_per_page'   => -1,
		'category_name'    => get_the_title($artist_id),
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_status'      => 'publish',
	);
	$newsposts = get_posts($args);
	$current_id = $post->ID;
	?>
	<nav class="submenu <?php if(is_singular('single-wil_work')){echo 'text-shadow';} ?>">
		<ul>
			<li><a href="#work" class="tab work">Work</a></li>
			<?php
			if (!empty($newsposts)):
			?>
				<li><a href="#news" class="tab news">News</a></li>
			<?php endif; ?>
			<li><a href="#text" class="tab text">Text</a></li>
			<li><a href="<?php echo get_the_permalink($artist_id); ?>#bio" class="show-bio">Bio</a></li>
		</ul>
	</nav>
	<div class="tab-content clearfix" id="work">
		<ul>
			<?php
				$query = new WP_Query( array( 'post_type' => 'wil_work', 'meta_query' => array( array('key' => 'wil_artist_id','value' => $artist_id ), ), 'posts_per_page' => -1, /*'orderby' => 'ID', 'order' => 'DESC', */'post_status' => 'publish' ) );

				while ( $query->have_posts() ) {
					$query->the_post();
					$current_class = ( $current_id == $post->ID ) ? 'class="active"' : '';
					echo '<li><h4><a href="'.get_the_permalink().'" '.$current_class.'>'.get_the_title().'</a></h4></li>';
				}
				wp_reset_postdata();
			?>
		</ul>
	</div>
	<?php
	if (!empty($newsposts)):
	?>
	<div class="tab-content clearfix" id="news">
		<ul>
			<?php
				foreach ($newsposts as $post) {
					setup_postdata($post);
					$current_class = ($current_id == $post->ID) ? 'class="active"' : '';
					echo '<li><h4><a href="'.get_the_permalink().'" '.$current_class.'>'.get_the_title().'</a></h4></li>';
				}
				wp_reset_postdata();
			?>
		</ul>
	</div>
	<?php endif; ?>
	<div class="tab-content clearfix" id="text">
		<ul>
			<?php
				$query = new WP_Query( array( 'post_type' => 'wil_text', 'meta_query' => array( array('key' => 'wil_artist_id','value' => $artist_id ), ), 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'post_status' => 'publish' ) );

				while ( $query->have_posts() ) {
					$query->the_post();
					$current_class = ( $current_id == $post->ID ) ? 'class="active"' : '';
					echo '<li><h4><a href="'.get_the_permalink().'" '.$current_class.'>'.get_the_title().'</a></h4></li>';
				}
				wp_reset_postdata();
			?>
		</ul>
	</div>

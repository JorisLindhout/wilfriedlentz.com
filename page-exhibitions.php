<?php
get_header();

function active_menu_item($current_anchor, $anchor) {
	return $current_anchor == $anchor ? " blue active" : "";
}

$page_title = "Exhibitions in the Gallery";
?>

<!-- exhibitions page -->
<?php
// Query all exhibitions, correctly ordering the exhibitions:
// First the current, then next, then past exhibitions.
// Additionally, "next" exhibitions should be sorted on their start date.
$anchors = array(1 => "now", 2 => "next", 3 => "past");
global $wpdb;

$current_time = strtotime('00:00:00');
$query_str = <<<SQL
SELECT $wpdb->posts.*,
       CASE WHEN %d BETWEEN meta_1.meta_value AND meta_2.meta_value
            THEN 1
            WHEN %d <= meta_1.meta_value
            THEN 2
            WHEN %d > meta_2.meta_value
            THEN 3
            ELSE 4
       END AS post_period
FROM $wpdb->posts
INNER JOIN $wpdb->postmeta AS meta_1 ON ($wpdb->posts.ID = meta_1.post_id)
INNER JOIN $wpdb->postmeta AS meta_2 ON ($wpdb->posts.ID = meta_2.post_id)
WHERE $wpdb->posts.post_type = 'wil_exhibition'
AND (meta_1.meta_key = 'wil_exhibition_start')
AND (meta_2.meta_key = 'wil_exhibition_end')
AND $wpdb->posts.post_status = 'publish'
ORDER BY post_period ASC,
         CASE post_period
         WHEN 2 THEN -1 * (meta_1.meta_value + 0)
         ELSE meta_2.meta_value + 0 END DESC
SQL;

$query_str = $wpdb->prepare($query_str, array_fill(0, 3, $current_time));
$posts = $wpdb->get_results($query_str);

$has_now = false;
$has_next = false;
// Loop through the posts up to the first one in "past" period, to determine
// whether we have any posts in the "now" or "next" period.
// This is needed to determine whether we should display anchors for "now" or "next".
foreach ($posts as $post) {
	$period = $post->post_period;
	if ($period > 2) {
		break;
	} else {
		$has_now = $has_now ?: ($period == 1);
		$has_next = $has_next ?: ($period == 2);
	}
}

foreach ($posts as $post) {
	setup_postdata($post);
	$anchor_id = '';
	$anchor_suffix = '';
	$anchor_index = $post->post_period;

	if (isset($anchors[$anchor_index])) {
		$anchor = $anchors[$anchor_index];
		$anchor_id = ' id="' . $anchor . '"';
		unset($anchors[$anchor_index]);
	}

	if (!empty($anchor_id)) {
		$anchor_suffix = "-head";
		$head_title = $page_title ?: $anchor;
		echo <<<EOHTML
		<section id="$anchor-head" class="page-head clearfix">
			<h2 class="title uppercase">$head_title</h2>
EOHTML;
		include(locate_template('exhibitions-submenu.php', false, false));
		echo <<<EOHTML
		</section>
EOHTML;
		$anchor_suffix = '';
		$page_title = false;
	}
?>

		<?php include(locate_template('exhibitions-submenu.php', false, false)) ?>
		<article<?= $anchor_id ?> class="exhibition clearfix">
			<figure class="responsive-figure">
				<a href="<?php the_permalink() ?>">
					<?php the_post_thumbnail('wil-medium'); ?>
					<p class="exhibition-info">Info</p>
				</a>
			</figure>
			<header>
				<a href="<?php the_permalink(); ?>">
					<?php include(locate_template('exhibition-title.php', false, false)) ?>
				</a>
			</header>
		</article>
<?php } ?>
	<!-- /exhibitions page  -->
<?php get_footer(); ?>

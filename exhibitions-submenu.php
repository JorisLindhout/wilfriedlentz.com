
			<nav class="submenu">
				<ul>
					<?php if ($has_now !== false): ?>
						<li><a href="#now<?= $anchor_suffix ?>" class="animated-scroll<?= active_menu_item($anchor, "now") ?>">Now</a></li>
					<?php endif; ?>
					<?php if ($has_next !== false): ?>
						<li><a href="#next<?= $anchor_suffix ?>" class="animated-scroll<?= active_menu_item($anchor, "next") ?>">Next</a></li>
					<?php endif; ?>
					<li><a href="#past<?= $anchor_suffix?>" class="animated-scroll<?= active_menu_item($anchor, "past") ?>">Past</a></li>
				</ul>
			</nav>

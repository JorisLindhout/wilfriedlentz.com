	</main>
	
	<!-- Footer -->
	<footer>
		
		<!-- Logo -->
		<div id="logo-footer" class="logo">
			<a href="/" title="home" class="blue uppercase">
				<h2><?php echo get_bloginfo( 'name' ); ?></h2>
			</a> 
		</div>
		<!-- /Logo -->
		
		<!-- Contact -->
		<div class="footer-contact">
			<?php echo wpautop(get_post_meta( 1361, 'wil_page_aside', true )); ?>
			<a href="https://btn.ymlp.com/xgewjshegmgj" target=_blank>Sign up for our newsletter</a>
		</div>
		<!-- /Contact -->
		<br class="clearfix">
	</footer>
	<!-- /Footer -->
	
	<?php wp_footer(); ?>

</body>
</html>

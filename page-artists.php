<?php get_header(); ?>

	<!-- artists page -->
	<section class="page-head">
		<h2 class="title uppercase"><?=the_title()?></h2>
		<br class="clearfix"/>
	</section>
	<ul id="artists-grid">
	<?php
		$query = new WP_Query( array( 'post_type' => 'wil_artist', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'post_status' => 'publish' ) );
		while ( $query->have_posts() ) {
			$query->the_post();
			echo '<li><a href="'.get_the_permalink().'">';
				the_post_thumbnail('wil-thumb');
				echo '<h3>'.get_the_title().'</h3>';
			echo '</a></li>';
		}
	?>
	</ul>
	
	<!-- /artists page  -->

<?php get_footer(); ?>

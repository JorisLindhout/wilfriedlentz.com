<?php
/*
 *  Author: Joris Lindhout & Skip Lentz
 *  URL: wilfriedlentz.com
 *  Custom functions, support, custom post types and more.
 */

/*
 * ========================================================================
 * External Modules/Files
 * ========================================================================
 */


/*
 * ========================================================================
 * Theme Support
 * ========================================================================
 */

if (function_exists('add_theme_support')){
    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');

    add_theme_support('automatic-feed-links');
    load_theme_textdomain(get_template_directory() . '/languages');
}


/*
 * ========================================================================
 * Functions
 * ========================================================================
 */

function wil_scripts(){
    if (!is_admin()) {
        wp_deregister_script('jquery'); // Deregister WordPress jQuery
        wp_register_script('jquery', '//code.jquery.com/jquery-3.1.1.min.js', array(), '3.1.1', true);
        wp_enqueue_script('jquery');

		wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3');
        wp_enqueue_script('modernizr');

        wp_register_script('wil_js', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true); // Custom scripts 
        wp_enqueue_script('wil_js');

		wp_register_script('flickity', 'https://unpkg.com/flickity@2/dist/flickity.pkgd.js', array('jquery'), '2.0', true);
		wp_register_script('slider', get_template_directory_uri() . '/js/slider.js', array('flickity'), '1.0.0', true); // Scroll snap polyfill 
		if (is_home()) {
			wp_enqueue_script('flickity');
			wp_enqueue_script('slider');
		}
    }
}

function register_wil_menus() {
	register_nav_menus( array(
		'main-menu' => 'Main Menu',
	) );
}

// Theme Stylesheets using Enqueue
function wil_styles(){
    wp_register_style('normalize', '//cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css', array(), '5.0.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!
    
    wp_register_style('wil_css', get_template_directory_uri() . '/style.css', array(), '1.1', 'all');
    wp_enqueue_style('wil_css'); // Enqueue it!
	wp_register_style('flickity', 'https://unpkg.com/flickity@2/dist/flickity.css', array(), '2.0', 'all');
	if (is_home()) {
		wp_enqueue_style('flickity');
	}

}

// Load Optimised Google Analytics in the footer
function wil_add_google_analytics(){
    $google = "<!-- Optimised Asynchronous Google Analytics -->";
    $google .= "<script>";
    $google .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-61248973-1', 'auto');
	  ga('send', 'pageview');";
    $google .= "</script>";
    echo $google;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function wil_css_attributes_filter($var){
    return is_array($var) ? array() : '';
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function wil_add_slug_to_body_class($classes){
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
		$classes[] = sanitize_html_class(get_query_var('args'));
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
		$classes[] = sanitize_html_class(get_query_var('args'));
    }

    return $classes;
}

// Remove 'text/css' from our enqueued stylesheet
function wil_style_remove($tag){
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

function wil_remove_header_info() {
	remove_action('wp_head', 'feed_links', 2);  //removes feeds
	remove_action('wp_head', 'feed_links_extra', 3);  //removes comment feed links
}


/*
 * ========================================================================
 * Actions + Filters + ShortCodes
 * ========================================================================
 */

// Add Actions
add_action('wp_enqueue_scripts', 'wil_scripts'); // Add Custom Scripts
add_action('wp_footer', 'wil_add_google_analytics'); // Google Analytics optimised in footer
add_action('wp_enqueue_scripts', 'wil_styles'); // Add Theme Stylesheet
add_action('init', 'wil_remove_header_info');
add_action('init', 'register_wil_menus');

// Remove Actions
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'wil_add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('style_loader_tag', 'wil_style_remove'); // Remove 'text/css' from enqueued stylesheet



// ADMIN AREA ****************************************************************************************************************************
function wil_thumb_size() {
	add_image_size('wil-thumb', 424, 267, true); 
	add_image_size('wil-medium', 1024);
	add_image_size('wil-large', 2500); 
}

// Add image sizes to Media Selection 
function wil_display_image_size_names_muploader( $sizes ) {
	$new_sizes = array();
	$added_sizes = get_intermediate_image_sizes();
	foreach( $added_sizes as $key => $value) {
		$new_sizes[$value] = $value;
	}
	$new_sizes = array_merge( $new_sizes, $sizes );
	return $new_sizes;
}

function wil_global_meta_box(){
    $screens = array('wil_artist', 'wil_exhibition');
    foreach ($screens as $screen){
        add_meta_box(
            'wil_global_meta_box',
            'Featured post',
            'wil_global_meta_box_callback',
            $screen,
			'side',
			'high'
        );
    }
	add_meta_box(
		'wil_global_post_meta_box',
		'Featured post',
		'wil_global_post_meta_box_callback',
		'post',
		'side',
		'high'
	);
}
function wil_global_meta_box_callback($post){
	wp_nonce_field('wil_global_meta_box_nonce', 'wil_global_meta_box_nonce');
	$wil_featured_post = get_post_meta($post->ID, 'wil_featured_post', true);
	$wil_featured_index = get_post_meta($post->ID, 'wil_featured_index', true);
	echo '<label>Feature post on homepage: </label> <input type="checkbox" name="wil_featured_post" '.($wil_featured_post === 'on' ? ' checked' : '').'>';
	echo '<br>';
	echo '<label>Featured post index: </label> <input type="number" name="wil_featured_index" value="'.($wil_featured_index ?: '0').'">';
}
function wil_save_global_meta_box_data( $post_id ) {
    if(!isset($_POST['wil_global_meta_box_nonce'])){
        return false;
    }

    if(!wp_verify_nonce($_POST['wil_global_meta_box_nonce'], 'wil_global_meta_box_nonce')){
        return false;
    }

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
        return false;
    }

	if(!current_user_can('edit_post', $post_id)){
		return false;
	}

	if(isset($_POST['wil_featured_post'])){
		$wil_featured_post = $_POST['wil_featured_post'];
	}else{
		$wil_featured_post = '';
	}
	if(isset($_POST['wil_featured_index'])){
		$wil_featured_index = $_POST['wil_featured_index'];
	}else{
		$wil_featured_index = 0;
	}
	update_post_meta($post_id, 'wil_featured_post', $wil_featured_post);
	update_post_meta($post_id, 'wil_featured_index', intval($wil_featured_index));

	return true;
}
function wil_global_post_meta_box_callback($post) {
	wil_global_meta_box_callback($post);
	$wil_featured_url = get_post_meta($post->ID, 'wil_featured_url', true);
	echo '<br>';
	echo '<label>External link: </label> <input type="url" name="wil_featured_url" value="'.($wil_featured_url ?: '').'">';
}
function wil_save_global_post_meta_box_data($post_id) {
	if (!wil_save_global_meta_box_data($post_id)) {
		return;
	}

	if (isset($_POST['wil_featured_url'])) {
		$wil_featured_url = esc_url_raw($_POST['wil_featured_url'], array('http', 'https'));
		update_post_meta($post_id, 'wil_featured_url', $wil_featured_url);
	}
}
function wil_featured_url_callback($url, $post_id) {
	$wil_featured_url = get_post_meta($post_id, 'wil_featured_url', true);
	if ($wil_featured_url && $wil_featured_url !== '') {
		return $wil_featured_url;
	}
	return $url;
}

function wil_page_meta_box(){
	add_meta_box(
		'wil_page_meta_box',
		'Aside',
		'wil_page_meta_box_callback',
		'page',
		'side',
		'high'
	);
}
function wil_page_meta_box_callback($post){
    wp_nonce_field('wil_page_meta_box_nonce', 'wil_page_meta_box_nonce');
    $wil_page_aside = get_post_meta($post->ID, 'wil_page_aside', true);
	wp_editor( $wil_page_aside, 'wil_page_aside');
}
function wil_save_page_meta_box_data( $post_id ) {
    if(!isset($_POST['wil_page_meta_box_nonce'])){
        return;
    }

    if(!wp_verify_nonce($_POST['wil_page_meta_box_nonce'], 'wil_page_meta_box_nonce')){
        return;
    }

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
        return;
    }

    if(!current_user_can('edit_post', $post_id)){
        return;
    }

	$wil_page_aside = $_POST['wil_page_aside'];
	update_post_meta($post_id, 'wil_page_aside', $wil_page_aside);
}

function wil_featured_post_order() {
	add_dashboard_page(
		'Featured post order',
		'Featured posts',
		'read',
		'wil_featured_post_order',
		'wil_featured_post_order_page'
	);
}
function wil_featured_post_order_page() {
	include_once(locate_template('featured_post_order.php', false, false));
}
function wil_featured_post_order_redirect() {
	if (!isset($_POST['_wp_http_referer'])) {
		$_POST['_wp_http_referer'] = wp_login_url();
	}
	$url = sanitize_text_field(
		wp_unslash( $_POST['_wp_http_referer'] ) // Input var okay.
	);

	// Finally, redirect back to the admin page.
	wp_safe_redirect(urldecode($url));
}
function wil_featured_post_order_save() {
	if(!isset($_POST['wil_featured_post_order_nonce'])){
		return;
	}

	if(!wp_verify_nonce($_POST['wil_featured_post_order_nonce'], 'wil_featured_post_order_nonce')){
		return;
	}

	foreach ($_POST['wil_featured_post'] as $post_id => $attr) {
		if(isset($attr['featured'])) {
			$wil_featured_post = $attr['featured'];
		} else {
			$wil_featured_post = '';
		}
		update_post_meta($post_id, 'wil_featured_post', $wil_featured_post);
		update_post_meta($post_id, 'wil_featured_index', intval($attr['index']));
	}

	wil_featured_post_order_redirect();
}

// Load custom js in admin
function wil_load_custom_js_admin() {
    wp_register_script('wil_admin_js', get_template_directory_uri() . '/js/admin.js', array('jquery'), '1.0.0', true); // Custom scripts 
	wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_script('wil_admin_js');
}

// Load custom css in admin
function wil_load_custom_css_admin() {
        wp_register_style('wil_admin_css', get_template_directory_uri() . '/css/admin.css', array(), '1.0', 'all');
	    wp_enqueue_style('wil_admin_css'); // Enqueue it!
}

//order posts by date
function wil_post_order_in_admin( $wp_query ) {
	if ( is_admin() ) {
		$post_type = $wp_query->query['post_type'];
    	$wp_query->set( 'orderby', 'date' );
    	$wp_query->set( 'order', 'DESC' );
  	}
}

function wil_remove_menus(){
	remove_menu_page( 'edit-comments.php' );
}

function wil_disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

function wil_disable_add_new() {
	// Hide sidebar link
	global $submenu;
	unset($submenu['edit.php?post_type=wil_artist'][10]);
}

function wil_change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'News';
	$submenu['edit.php'][5][0] = 'News';
	$submenu['edit.php'][10][0] = 'Add News';
	$submenu['edit.php'][16][0] = 'News Tags';
	echo '';
}
function wil_change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'News';
	$labels->singular_name = 'News';
	$labels->add_new = 'Add News';
	$labels->add_new_item = 'Add News';
	$labels->edit_item = 'Edit News';
	$labels->new_item = 'News';
	$labels->view_item = 'View News';
	$labels->search_items = 'Search News';
	$labels->not_found = 'No News found';
	$labels->not_found_in_trash = 'No News found in Trash';
}

// Filter 'works' by artist
function wil_admin_posts_filter_restrict_manage_posts(){
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    if ('wil_work' == $type){
        $values = array();
		$artists = get_posts(array('post_type'=> 'wil_artist','post_status'=> 'publish','posts_per_page'=>-1, 'orderby' => 'title','order'=> 'ASC'));
        foreach ($artists as $a) {
			$values += [ $a->post_title => $a->ID ];
        }
        ?>
        <select name="order_work_by_artist">
        <option value=""><?php _e('Filter By ', 'wil'); ?></option>
        <?php
            $current_v = isset($_GET['order_work_by_artist'])? $_GET['order_work_by_artist']:'';
            foreach ($values as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_v? ' selected="selected"':'',
                        $label
                    );
                }
        ?>
        </select>
        <?php
    }
}
function wil_posts_filter( $query ){
    global $pagenow;
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
    if ( 'wil_work' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['order_work_by_artist']) && $_GET['order_work_by_artist'] != '') {
        $query->query_vars['meta_key'] = 'wil_artist_id';
        $query->query_vars['meta_value'] = $_GET['order_work_by_artist'];
    }
}

function wil_disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'wil_disable_emojicons_tinymce' );
}

// Add Filters & Actions
add_filter('pre_get_posts', 'wil_post_order_in_admin' );
add_filter('image_size_names_choose', 'wil_display_image_size_names_muploader', 11, 1);
add_filter( 'parse_query', 'wil_posts_filter' );

add_filter('wil_featured_permalink', 'wil_featured_url_callback', 10, 2);

add_action('after_setup_theme','wil_thumb_size');
add_action('admin_enqueue_scripts', 'wil_load_custom_js_admin');
add_action('admin_enqueue_scripts', 'wil_load_custom_css_admin');
add_action('init', 'wil_disable_wp_emojicons');
add_action('admin_menu', 'wil_disable_add_new');
add_action('admin_menu', 'wil_change_post_menu_label');
add_action('init', 'wil_change_post_object_label');
add_action('admin_menu', 'wil_remove_menus');
add_action('add_meta_boxes', 'wil_global_meta_box');
add_action('save_post', 'wil_save_global_meta_box_data');
add_action('save_post', 'wil_save_global_post_meta_box_data');
add_action('add_meta_boxes', 'wil_page_meta_box');
add_action('save_post', 'wil_save_page_meta_box_data');
add_action('admin_menu', 'wil_featured_post_order');
add_action('admin_post_wil_featured_post_order_save', 'wil_featured_post_order_save');
add_action( 'restrict_manage_posts', 'wil_admin_posts_filter_restrict_manage_posts' );


// AJAX CALLS ****************************************************************************************************************************

function remove_media_callback(){
	if(isset($_POST['id'])){
		$id = $_POST['id'];
	}else{
		exit;
	}

	if ( current_user_can('upload_files', $id) ) {

		wp_delete_attachment( $id );

		echo 'File deleted!';

	}else{

		echo 'User is not allowed to do this!';
	}
	
	wp_die();
}

add_action( 'wp_ajax_remove_media', 'remove_media_callback' );
add_action( 'wp_ajax_nopriv_remove_media', 'remove_media_callback' );

?>

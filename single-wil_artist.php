<?php get_header(); ?>

	<?php if ( have_posts() ) : the_post(); ?>
		<aside class="artist-menu">
			<h2 class="title uppercase"><?php the_title(); ?></h2>
			<?php
			$artist_id = get_the_ID();
			include(locate_template('single-artist-menu.php', false, false));
			?>
		</aside>
		<!-- single artist page -->
		<figure id="cover">
			<?php
			the_post_thumbnail('wil-large');
			?>
			<figcaption>
				<?php
				the_post_thumbnail_caption();
				?>
			</figcaption>
		</figure>
		<article id="bio" class="artist-page-content">
			<div class="article-content">
				<?php the_content(); ?>
			</div>
		</article>
		<br class="clearfix">
		<!-- /single artist page  -->
	
	<?php endif; ?>

<?php get_footer(); ?>

<?php get_header(); ?>

<!-- single page -->
<article>
	<?php if ( have_posts() ) : the_post(); ?>
		<figure class="responsive-figure">
			<?php the_content(); ?>
		</figure>
		<header>
			<h2 class="title uppercase"><?php the_title(); ?></h2>
			<aside>
				<?php echo wpautop(get_post_meta( get_the_ID(), 'wil_page_aside', true )); ?>
				<script type="text/javascript" src="https://signup.ymlp.com/signup.js?id=gewjshegmgj"></script>
			</aside>
		</header>
		<br class="clearfix">
	<?php endif; ?>
</article>
<!-- /single page  -->

<?php get_footer(); ?>

<?php get_header(); ?>

	<!-- single page -->
	<?php if ( have_posts() ) : the_post(); ?>
		<article>
			<figure><?php the_post_thumbnail('wil-medium') ?></figure>
			<header>
				<h2 class="title uppercase"><?php the_title(); ?></h3>
					<?php echo wpautop(get_post_meta( get_the_ID(), 'wil_page_aside', true )); ?>
					<?php if(is_page(1361)) { ?>
						<script type="text/javascript" src="https://signup.ymlp.com/signup.js?id=gewjshegmgj"></script>
					<?php } ?>
			</header>
			<div class="article-content">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endif; ?>
	<!-- /single page  -->

<?php get_footer(); ?>
